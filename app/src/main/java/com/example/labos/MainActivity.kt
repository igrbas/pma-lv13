package com.example.labos

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {

    private lateinit var sendBtn: Button;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sendBtn = findViewById(R.id.button);

        sendBtn.setOnClickListener {
            Intent(Intent.ACTION_SEND).apply{
                type="text/plain"
                putExtra(Intent.EXTRA_TEXT, getString(R.string.message))
            }.also {
                intent ->
                val chooserIntent = Intent.createChooser(intent, getString(R.string.title))
                startActivity(chooserIntent)
            }
        }
    }
}